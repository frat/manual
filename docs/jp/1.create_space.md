# スペースを作成する
トップページよりスペースを作成することができます。
![create space](../img/jp/create_space.gif)

メールアドレスを入力すると、スペースにログインするためのコードが送信されます。
![login to space](../img/jp/login_to_space.png)

メール中のリンクをクリックするか、コードを入力してください。
![login to space](../img/jp/login_to_space.gif)

これでスペースが作成されます。
