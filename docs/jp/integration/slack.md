# Slackと連携する。
SlackとfRatを連携させることで、fRatのチケットをSlack上の会話にリンクさせることができます。
リンクさせることでチケットの検討内容を追えるようにします。

## fRatでWebhook URLを生成する。
グループ設定を編集、システム連携でSlackを選択し、追加、保存します。URLが生成されるので、保持しておきます。
![Slack連携](../../img/jp/integrate_with_slack.gif)

## SlackでOutgoing Webhookを設定する。
[SlackのOutgoing Webhook設定ページ](https://www.slack.com/apps/A0F7VRG6Q-outgoing-webhook)から、Outgoing Webhook インテグレーションの追加ボタンを押し、引き金となる言葉に「fRat」、URLに先程取得したURL、名前をカスタマイズに「fRat」と入力、設定を保存してください。Slackにログインしていない場合はログインしてから行ってください。

![Slack連携](../../img/jp/integrate_slack.gif)

これによりSlack上で
fRat linkto {チケットキー} {コメント}
と入力すると、チケットキー（グループキー + - + 番号）を持つチケットにSlackへのリンクが付与されます。

関係するコミュニケーションをチケットに紐付けておくことで、検討の経緯を記録することができます。

※ SlackはSlack Technologies,Incの商標または登録商標です