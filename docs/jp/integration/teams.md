# Teamsと連携する。
TeamsとfRatを連携させることで、fRatのチケットをTeams上の会話にリンクさせることができます。
リンクさせることでチケットの検討内容を追えるようにします。

## fRatでWebhook URLを生成する。
グループ設定を編集、システム連携でMicrosoft Teamsを選択、追加ボタンを押します。
候補URLが得られるのでしゅとくしてください。
そして、このページはそのままにして、保存前に、Teamsからセキュリティートークンを取得します。
![Teams連携](../../img/jp/integrate_with_teams1.gif)

## TeamsでOutgoing Webhookを設定する。
Teamsのチームの設定、アプリ、から、送信Webhookを押し、名前に「fRat」、コールバック先程取得したURL、説明に「fRat」と入力、設定を保存してください。セキュリティートークンが取得できるのでコピーしてください。
![Teams連携](../../img/jp/integrate_with_teams2.gif)

## fRatにセキュリティートークンを入力、設定を保存する。
fRatに戻り、Teamsで得られたセキュリティートークンを入力、設定を追加、保存してください。
![Teams連携](../../img/jp/integrate_with_teams3.gif)


これによりTeams上で
@fRat linkto {チケットキー} {コメント}
と入力すると、チケットキー（グループキー + - + 番号）を持つチケットにTeamsへのリンクが付与されます。

関係するコミュニケーションをチケットに紐付けておくことで、検討の経緯を記録することができます。

※ TeamsはMicrosoft Corporationの商標または登録商標です