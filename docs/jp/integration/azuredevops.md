# Azure DevOpsと連携する。
Azure DevOpsと連携することで、Pull Request がマージされた際にfRatのチケットを100%にすることができます。

## fRatでWebhook URLを生成する。
グループ設定を編集、システム連携でAzure DevOpsを選択し、追加、保存します。URLが生成されるので、保持しておきます。
![Azure DevOps連携](../../img/jp/integrate_with_azuredevops1.gif)

## Azure DevOpsのリポジトリにWebhookを設定する。
Azure DevOpsのリポジトリでProject Settings(左メニューの歯車), Service hooksを選択、Web hooksを選択します。

Nextから"Trigger on this type of event"に"Pull Request Updated"、Changeに"Status Changed"を選択します。
Nextを押して、URLにRatで取得したURLを入力します。
Finishで、設定が完了です。
![Azure DevOps連携](../../img/jp/integrate_with_azuredevops2.gif)
