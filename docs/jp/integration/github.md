# GitHubと連携する。
GitHubと連携することで、Pull Request がマージされた際にfRatのチケットを100%にすることができます。

## fRatでWebhook URLを生成する。
グループ設定を編集、システム連携でGitHubを選択し、追加、保存します。URLが生成されるので、保持しておきます。
![GitHub連携](../../img/jp/integrate_with_github1.gif)

## GitHubのリポジトリにWebhookを設定する。
GitHubのリポジトリでSettings, Webhooksを選択、Add Webhookを押します。
Payload URLにfRatで取得したURLを入力し、Let me select Individual events.を選択、Pull Requestにチェックを入れてください。
Add Webhookで、設定が完了です。
![GitHub連携](../../img/jp/integrate_with_github2.gif)
