# GitLabと連携する。
GitLabと連携することで、Merge Request がマージされた際にfRatのチケットを100%にすることができます。

## fRatでWebhook URLを生成する。
グループ設定を編集、システム連携でGitLabを選択し、追加、保存します。URLが生成されるので、保持しておきます。
![GitLab連携](../../img/jp/integrate_with_gitlab1.gif)

## GitLabのリポジトリにWebhookを設定する。
GitLabのプロジェクトでSettings(左メニューの歯車)から、Webhooksを選択します。

URLにRatで取得したURLを入力し、TriggerにMerge request eventsを選択します。
Add Webhookで、設定が完了です。
![GitLab連携](../../img/jp/integrate_with_gitlab2.gif)
